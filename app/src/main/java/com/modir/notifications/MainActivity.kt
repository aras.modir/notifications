package com.modir.notifications

import android.app.PendingIntent
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.media.session.MediaSessionCompat
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.modir.notifications.App.Companion.CHANNEL_1_ID
import com.modir.notifications.App.Companion.CHANNEL_2_ID

class MainActivity : AppCompatActivity() {

    private lateinit var notificationManagerCompat: NotificationManagerCompat
    private lateinit var editTextTitle: EditText
    private lateinit var editTextMessage: EditText
    private lateinit var mediaSession: MediaSessionCompat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bind()
        notificationManagerCompat = NotificationManagerCompat.from(this)
        mediaSession = MediaSessionCompat(this, "tag")
    }

    fun sendOnChannel1(view: View) {
        val title = editTextTitle.text.toString()
        val message = editTextMessage.text.toString()

        val activityIntent = Intent(this, MainActivity::class.java)
        val contentIntent = PendingIntent.getActivity(this,
                0, activityIntent, 0)

//        val broadCastIntent = Intent(this, NotificationReceiver::class.java)
//        broadCastIntent.putExtra("toastMessage", message)
//        val actionIntent = PendingIntent.getBroadcast(this, 0, broadCastIntent,
//                PendingIntent.FLAG_UPDATE_CURRENT)

//        val largeIcon = BitmapFactory.decodeResource(resources, R.drawable.index)
        val picture = BitmapFactory.decodeResource(resources, R.drawable.test)

        val notification = NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_one)
                .setContentTitle(title)
                .setContentText(message)
                .setLargeIcon(picture)
                .setStyle(NotificationCompat.BigPictureStyle()
                        .bigPicture(picture)
                        .bigLargeIcon(null))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .build()

        notificationManagerCompat.notify(1, notification)


    }

    fun sendOnChannel2(view: View) {

        val title = editTextTitle.text.toString()
        val message = editTextMessage.text.toString()


        val artwork = BitmapFactory.decodeResource(resources, R.drawable.test)

        val notification = NotificationCompat.Builder(this, CHANNEL_2_ID)
                .setSmallIcon(R.drawable.ic_two)
                .setContentTitle(title)
                .setContentText(message)
                .setLargeIcon(artwork)
                .addAction(R.drawable.ic_dislike, "Dislike", null)
                .addAction(R.drawable.ic_previouse, "Previous", null)
                .addAction(R.drawable.ic_pause, "Pause", null)
                .addAction(R.drawable.ic_next, "Next", null)
                .addAction(R.drawable.ic_like, "Like", null)
                .setStyle(androidx.media.app.NotificationCompat.MediaStyle()
                        .setShowActionsInCompactView(1, 2, 3)
                        .setMediaSession(mediaSession.sessionToken))
                .setSubText("Sub Text")
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .build()

        notificationManagerCompat.notify(2, notification)
    }

    private fun bind() {

        editTextTitle = findViewById(R.id.edit_text_title)
        editTextMessage = findViewById(R.id.edit_text_message)
    }
}